(function ($) {
  Drupal.behaviors.ajax_easy =  {
    attach: function(context, settings) {
       var urlToCall = '';
       var baseURL = Drupal.settings.ajax_easy.base_url;
       var basePath = Drupal.settings.ajax_easy.path;
       var outType = Drupal.settings.ajax_easy.out_type;
       var ajaxEasyBase = baseURL + '/' + basePath + '/';

       $( '.ajax-easy', context ).click( function () {
         var thePath = $( '.ajax-easy' ).data( 'ajax-easy' );
         if (thePath !== '') {
            var urlToCall = ajaxEasyBase + thePath;
            var theResultid = $( '.ajax-easy' ).data( 'ajax-easy-target-id' );
            if ( theResultid !== '' && thePath !== '' ) {
                theResultid = '#' + theResultid;

                if ( outType == 'raw' ) {
                    var dataType = 'html';
                } else {
                    var dataType = 'json';
                }
                $.ajax({
                    type: 'GET',
                    url: urlToCall,
                    dataType: 'html',
                    // data: data,
                    success:function(html) {
                        if ( outType == 'raw' ) {
                            $( theResultid ).html( html );
                        } else {
                            var json = JSON.parse(html)
                            var items = [];
                            $.each( json , function( key, val ) {
                                items.push( "<li id='" + key + "'>" + val + "</li>" );
                            });

                            $( "<ul/>", {
                              "class": "my-new-list",
                              html: items.join( '' )
                            }).appendTo( theResultid );
                           // });

                        }
                    }
                });

            }

         }

       });

    }
  };
})(jQuery);