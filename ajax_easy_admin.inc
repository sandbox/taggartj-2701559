<?php

/**
 * Implements hook_form(). // admin settings form
 */
function ajax_easy_admin_form($form, &$form_state) {
  $form['set_rout'] = array(
    '#type' => 'textfield',
    '#title' => t('Default path'),
    '#default_value' => variable_get('ajax_easy_rout', 'ajax_easy'),
    '#description' => t('Note: changing this value will cause a cache flush'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $options = array(0 => t('Raw'), 1 => t('Json'));

  $form['set_output'] = array(
    '#type' => 'radios',
    '#title' => t('Set Out Put'),
    '#default_value' => variable_get('ajax_easy_set_output', 0),
    '#options' => $options,
    '#description' => t('Note: this will be raw output if not ticked.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

/**
 * Implementation of hook_FORM_form_submit().
 */
function ajax_easy_admin_form_submit($form, $form_state) {
  $values = $form_state['values'];
  $raw_set_rout = $values['set_rout'];
  $raw_set_output = $values['set_output'];

  if ($raw_set_rout !== 'ajax_easy') {
    //check does not contain strange char.
    if (strpos($raw_set_rout, '/') !== false) {
      return drupal_set_message(t('Please remove the "/" from your "Default path"'), 'error', FALSE);
    }
    $raw_set_rout =  preg_replace("~\b(?:https?://(?:www\.)?|www\.)\S+|[^A-Za-z0-9 ']~", '', $raw_set_rout);
    $raw_set_rout =  preg_replace('/ +/', '', $raw_set_rout);
    $safe_ish_string = check_plain(strtolower($raw_set_rout));

    //update var assumes admins want there site to work
    variable_set('ajax_easy_rout', $safe_ish_string);
    //clear menu cache to update new path
    $clear = menu_rebuild();
  }

  if (!is_numeric($raw_set_output)) {
    return drupal_set_message(t('Please don\'t hack the radios'), 'error', FALSE);
  }

  if ($raw_set_output == 0 || $raw_set_output == 1) {
    variable_set('ajax_easy_set_output', $raw_set_output);
  }
  drupal_set_message(t('Values have been updated'), 'status', FALSE);
}
